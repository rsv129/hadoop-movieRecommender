/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;

import java.util.LinkedList;

/**
 * 
 * @author ravi
 */
public class Scorer {

	double cosine;
	double dice;
	String str1;
	String str2;

	public Scorer(String one, String two) {
		str1 = one;
		str2 = two;
	}

	public Scorer() {

	}

	public void setStrings(String one, String two) {
		str1 = one;
		str2 = two;
	}

	public void calc()
    {
    	try
    	{
    		Constants cons = new Constants();
    		LinkedList<VectorData> matrix=Scores.constructVectorMatrix(cons.getRoots(str1), cons.getRoots(str2));
    		dice=Scores.dice(matrix);
    		cosine=Scores.cosine(matrix);
    	}
    	catch(java.lang.NoClassDefFoundError e)
    	{
    		LinkedList<VectorData> matrix=Scores.constructVectorMatrix(str1.split("[ ]") , str2.split("[ ]"));
    		dice=Scores.dice(matrix);
    		cosine=Scores.cosine(matrix);
    	}
    }

	public double getCosine() {
		return cosine;
	}

	public double getDice() {
		return dice;
	}

}
