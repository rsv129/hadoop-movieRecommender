/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movierecommender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author ravi
 */
public class JSONParser {
    
    String url;
    imdbMovie userMovie;
    
    public JSONParser(String u , imdbMovie movie)
    {
        url = u;
        userMovie = movie;
    }
    
    
    private static String readAll(Reader rd) throws IOException 
    {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) 
        {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    
    
    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException 
    {
        InputStream is = new URL(url).openStream();
        try 
        {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        }
        finally 
        {
            is.close();
        }
    }
    
    void getJson() throws IOException 
    {
        JSONObject json = readJsonFromUrl(url);
        //System.out.println(json.toString());
        String msg = json.getString("message");
        if(msg.compareTo("Movie found")==0)
        {
            JSONObject data = json.getJSONObject("data");
            System.out.println(data);
            float r = Float.parseFloat(data.getString("rating"));
            String plot = data.getString("plot");
            String n = data.getString("name");
            String genres = data.getString("genre");
            System.out.println("Name is " + n + " Rating is " + r + " Genres are " + genres);
            System.out.println("Plot : "+plot);
            userMovie.imdbMovieInit(n, r,plot);
            userMovie.addGenres(genres);
            System.out.println(userMovie.genres);
        }
        
    
    
    }
    
}
