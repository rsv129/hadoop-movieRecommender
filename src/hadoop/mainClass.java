package hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class mainClass extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		//args[0] = new String("4");
//    	System.setProperty("http.proxySet", "true");
//    	System.setProperty("https.proxySet", "true");
    	System.setProperty("java.net.useSystemProxies", "true");
//    	System.setProperty("https.proxyHost", "172.16.19.10"); 
//    	System.setProperty("https.proxyPort", "80"); 
		args[0] = new String("/user/140911058/project/imdb"); //input file
		args[1] = new String("/user/140911058/project/output8"); //final output :: output for mapred 2
		args[2] = new String("/user/140911058/project/intermediate8"); //output for mapred 1
		int res = ToolRunner.run(new Configuration(), new mainClass(), args);
		System.exit(res);

	}

	public int run(String[] args) throws Exception {
		Configuration c = new Configuration();
		String[] files = new GenericOptionsParser(c, args).getRemainingArgs();
		Path p1 = new Path(files[0]);
		Path p2 = new Path(files[1]);
		Path p3 = new Path(files[2]);
//		Path p4 = new Path("/user/140911058/project/intermediatea6");
//		String name = args[3];
		FileSystem fs = FileSystem.get(c);
		c.set("Genres", "Action,Drama,Mystery,Thriller");
		c.set("Plot", "Wounded to the brink of death and suffering from amnesia. Jason Bourne is rescued at sea by a fisherman. With nothing to go on but a Swiss bank account number. he starts to reconstruct his life. but finds that many people he encounters want him dead. However. Bourne realizes that he has the combat and mental skills of a world-class spy – but who does he work for?");
//		c.set("name", args[3]);
		if (fs.exists(p3)) {
			fs.delete(p3, true);
		}
//		Job job0 = new Job(c, "Movie Finder");
//
//		
//		job0.setJarByClass(mainClass.class);
//		MultipleInputs.addInputPath(job0, p1, TextInputFormat.class,
//				movieMapper.class);
//		job0.setReducerClass(movieReducer.class);
//		job0.setOutputKeyClass(Text.class);
//		job0.setOutputValueClass(Text.class);
//		FileOutputFormat.setOutputPath(job0, p4);
		
		Job job1 = new Job(c, "Genre Job");
		Job job2 = new Job(c, "Cluster Job");
		job1.setJarByClass(mainClass.class);
		MultipleInputs.addInputPath(job1, p1, TextInputFormat.class,
				genreMapper.class);
		job1.setReducerClass(reducer.class);
		job1.setOutputKeyClass(Text.class);
		job1.setOutputValueClass(Text.class);
		FileOutputFormat.setOutputPath(job1, p3);
		MultipleInputs.addInputPath(job2, p3, TextInputFormat.class,
				textCluster.class);
		job2.setReducerClass(textReducer.class);
		job2.setOutputKeyClass(Text.class);
		job2.setOutputValueClass(DoubleWritable.class);
		FileOutputFormat.setOutputPath(job2, p2);
		
		//boolean suc = job0.waitForCompletion(true);
		boolean success = job1.waitForCompletion(true);
		boolean successful = job2.waitForCompletion(true);
		return (success&successful ? 0 : 1);
	}

}