package hadoop;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import cluster.*;

public class textCluster extends
		Mapper<LongWritable, Text, Text, DoubleWritable> {

	public void map(LongWritable k, Text value, Context context)
			throws IOException, InterruptedException {
		Configuration c = context.getConfiguration();
		String plot = c.get("Plot");
		String line = value.toString();
		String[] words = line.split("\t");
		try{
			Scorer scores = new Scorer(plot,words[1]);
			scores.calc();
			DoubleWritable cosine = new DoubleWritable(scores.getCosine());
			if(cosine.get()>0.3)
				context.write(new Text(words[0]), cosine);
		}
		catch(java.lang.ArrayIndexOutOfBoundsException e)
		{
			//do nothing
		}
		
		
		
	}
}
