package hadoop;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class reducer extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Text value, Context context)
			throws IOException, InterruptedException {	
		context.write(key, value);
	}
}