package hadoop;

import java.io.IOException;
import java.util.ArrayList;

import javax.security.auth.login.Configuration;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
//import movierecommender.imdbMovie;

public class genreMapper extends Mapper<LongWritable, Text, Text, Text> {
	public void map(LongWritable key, Text value, Context con)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] words = line.split(",");
		org.apache.hadoop.conf.Configuration conf = con.getConfiguration();
		String param = conf.get("Genres");
		System.out.println(param);
		String[] userGenres = param.split(",");
		String[] curGenres = genreParser(words[1]).split(",");
		//System.out.println(words[0]+":"+words[1]);
		int count = 0;
		int flag = 0;
		for(int i=0 ; i<curGenres.length ; i++)
		{
			for(int j=0 ; j<userGenres.length ; j++)
			{
				if(curGenres[i].contains(userGenres[j]))
				{
					count++;
					if(j==0)
						flag=1;
				}
			}
		}
		if(count==userGenres.length)
		{
			//System.out.println("hi");
			con.write(new Text(words[6]), new Text(words[7]));
			
		}
		else if(userGenres.length>1&&count==userGenres.length-1)
		{
			con.write(new Text(words[6]), new Text(words[7]));
		}
		
	}
	
    String genreParser(String genres)
    {
        ArrayList<String> generatedGenres = new ArrayList();
        String returnString = new String();
        int colonArray[] = new int[100];
        int colonCount = -1;
        for(int i=0 ; i<genres.length() ; i++)
        {
            if(genres.charAt(i)==':')
                colonArray[++colonCount] = i;
        }
        for(int i=1,j=0 ; i<=colonCount ; i=i+2,j++)
        {
            int k=genres.indexOf("}", colonArray[i]);
            generatedGenres.add(genres.substring(colonArray[i]+2, k));      
        }
        for(int i=0 ; i<generatedGenres.size() ; i++)
        {
            String toadd = generatedGenres.get(i).replace("\"\"","");
            returnString += toadd;
            returnString += ",";
            //System.out.println(generatedGenres.get(i));
        }
        System.out.println(returnString);
        return returnString;
    }
	
}