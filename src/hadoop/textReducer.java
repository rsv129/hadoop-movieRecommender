package hadoop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class textReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

	Map<String,Double> output = new HashMap<String,Double>();
	
	public void reduce(Text key, DoubleWritable value, Context context)
			throws IOException, InterruptedException {
		if(value.get()>0.25)
			context.write(key,value);
	}
	
	
	public void cleanup(Context context) throws IOException, InterruptedException{ 
	    //Cleanup is called once at the end to finish off anything for reducer
	    //Here we will write our final output
		
		Comparator<Double> defaultComparator = new Comparator<Double>() {
			   @Override
			   public int compare(Double o1, Double o2) {
			       return o1.compareTo(o2);
			   }
			};
		
	     Map<String , Double>  sortedMap = new HashMap<String , Double>(output);
	     Collection<Double> valueset = sortedMap.values();
	     List<Double> list = new ArrayList(valueset);
	     Collections.sort(list, defaultComparator);
	     	for (Map.Entry<String, Double> entry : output.entrySet())
	     	{
	     		if(entry.getValue().compareTo((Double)0.25)==1) {
	     			context.write(new Text(entry.getKey()), new DoubleWritable(entry.getValue()));
	     		}
	     	}


	}
	
	
}